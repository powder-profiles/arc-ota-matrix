#!/usr/bin/env python

import os

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.igext as ig
import geni.rspec.emulab.pnext as pn
import geni.rspec.emulab as emulab
import geni.rspec.emulab.lanext as lanext


tourDescription = """
### ARC-OTA Development Profile

"""

tourInstructions = """

"""

BIN_PATH = "/local/repository/bin"
ETC_PATH = "/local/repository/etc"
UBUNTU_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
COTS_UE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-jammy-image"
COMP_MANAGER_ID = "urn:publicid:IDN+emulab.net+authority+cm"
DEFAULT_SRSRAN_HASH = "a15950301c5f3a1a166b79bb6c9ee901a4e8c2dd"
OPEN5GS_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-open5gs.sh")
SRSRAN_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-srsran.sh")

NODE_IDS = {
    "ru1": "bru-650-3",
    "ue": "nuc22",
    "mon": "nuc16",
}
MATRIX_GRAPH = {
    "ru1": ["ue", "mon"],
    "mon": ["ru1"],
    "ue": ["ru1"],
}
MATRIX_INPUTS = ["ru1"]
RF_IFACES = {}
RF_LINK_NAMES = {}
for k, v in MATRIX_GRAPH.items():
    RF_IFACES[k] = {}
    for node in (v):
        RF_IFACES[k][node] = "{}_{}_rf".format(k, node)
        if k in MATRIX_INPUTS:
            RF_LINK_NAMES["rflink_{}_{}".format(k, node)] = []


for k, v in MATRIX_GRAPH.items():
    if k in MATRIX_INPUTS:
        for node in (v):
            RF_LINK_NAMES["rflink_{}_{}".format(k, node)].append(RF_IFACES[k][node])
            RF_LINK_NAMES["rflink_{}_{}".format(k, node)].append(RF_IFACES[node][k])


pc = portal.Context()

node_types = [
    ("d430", "Emulab, d430"),
    ("d740", "Emulab, d740"),
]
pc.defineParameter(
    name="sdr_nodetype",
    description="Type of compute node paired with the SDRs",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[1],
    legalValues=node_types
)

pc.defineParameter(
    name="cn_nodetype",
    description="Type of compute node to use for CN node (if included)",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types
)

pc.defineParameter(
    name="sdr_compute_image",
    description="Image to use for compute connected to SDRs",
    typ=portal.ParameterType.STRING,
    defaultValue="",
    advanced=True
)

pc.defineParameter(
    name="srsran_commit_hash",
    description="Commit hash for srsRAN",
    typ=portal.ParameterType.STRING,
    defaultValue="",
    advanced=True
)

pc.defineParameter(
    name="cudu_compute_id",
    description="Component ID for compute node connected to RU",
    typ=portal.ParameterType.STRING,
    defaultValue="pc01-meb",
)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

node_name = "cn5g"
cn_node = request.RawPC(node_name)
cn_node.component_manager_id = COMP_MANAGER_ID
cn_node.hardware_type = params.cn_nodetype
cn_node.disk_image = UBUNTU_IMG
cn_if = cn_node.addInterface("{}-if".format(node_name))
cn_if.addAddress(pg.IPv4Address("192.168.1.1", "255.255.255.0"))
cn_link = request.Link("{}-link".format(node_name))
cn_link.setNoBandwidthShaping()
cn_link.addInterface(cn_if)
cn_node.addService(pg.Execute(shell="bash", command=OPEN5GS_DEPLOY_SCRIPT))
cn_node.addService(pg.Execute(shell="bash", command="/local/repository/bin/install-improved-iperf3.sh"))
cn_node.addService(pg.Execute(shell="bash", command="/local/repository/bin/start-iperf.sh"))
cn_node.addService(pg.Execute(shell="bash", command="/local/repository/bin/install-vsftpd.sh.sh"))

node_name = "cudu"
cudu = request.RawPC(node_name)
cudu.component_manager_id = COMP_MANAGER_ID
cudu.component_id = params.cudu_compute_id
cudu.disk_image = UBUNTU_IMG
cudu_cn_if = cudu.addInterface("{}-cn-if".format(node_name))
cudu_cn_if.component_id = "eth6"
cudu_cn_if.addAddress(pg.IPv4Address("192.168.1.2", "255.255.255.0"))
cn_link.addInterface(cudu_cn_if)

duru1mgmt = cudu.addInterface("{}ru1mgmt".format(node_name))
duru1mgmt.component_id = "eth4"
duru1mgmt.addAddress(pg.IPv4Address("10.13.1.1", "255.255.255.0"))
duru1mgmt.PTP()
duru2mgmt = cudu.addInterface("{}ru2mgmt".format(node_name))
duru2mgmt.component_id = "eth5"
duru2mgmt.addAddress(pg.IPv4Address("10.13.2.1", "255.255.255.0"))

duru1ofh = cudu.addInterface("{}ru1ofh".format(node_name))
duru1ofh.component_id = "eth4"
duru2ofh = cudu.addInterface("{}ru2ofh".format(node_name))
duru2ofh.component_id = "eth5"

##### install NVIDIA SDK #####

cudu.addService(pg.Execute(shell="bash", command="sudo /local/repository/bin/setup-ptp.sh"))
cudu.addService(pg.Execute(shell="bash", command="/local/repository/bin/update-attens bru1 0"))
cudu.addService(pg.Execute(shell="bash", command="/local/repository/bin/update-attens bru2 95"))

# collect node objects for RF matrix
matrix_nodes = {}

# benetel RU 1
node_name = "ru1"
ru1 = request.RawPC(node_name)
ru1.component_manager_id = COMP_MANAGER_ID
ru1.component_id = NODE_IDS[node_name]
ru1dumgmt = ru1.addInterface("{}dumgmt".format(node_name))
ru1dumgmt.component_id = "eth0"
ru1dumgmt.addAddress(pg.IPv4Address("10.13.1.2", "255.255.255.0"))
ru1dumgmt.PTP()
ru1dumgmt.SyncE()
ru1duofh = ru1.addInterface("{}duofh".format(node_name))
ru1duofh.component_id = "eth0"
duru1u = request.Link("duru1u", members=[duru1mgmt, ru1dumgmt])
duru1u.DualModeTrunking()
duru1t = request.Link("duru1t", members=[duru1ofh, ru1duofh])
duru1t.setVlanTag(23)
duru1t.DualModeTrunking(duru1u)
ru1.Desire("rf-controlled", 1)
matrix_nodes[node_name] = ru1

# COTS UE
node_name = "ue"
ue = request.RawPC(node_name)
ue.component_manager_id = COMP_MANAGER_ID
ue.component_id = NODE_IDS[node_name]
ue.disk_image = COTS_UE_IMG
ue.Desire("rf-controlled", 1)
ue.addService(pg.Execute(shell="bash", command="/local/repository/bin/module-airplane.sh"))
ue.addService(pg.Execute(shell="bash", command="/local/repository/bin/setup-cots-ue.sh internet"))
matrix_nodes[node_name] = ue

# monitor node
node_name = "mon"
mon = request.RawPC(node_name)
mon.component_manager_id = COMP_MANAGER_ID
mon.component_id = NODE_IDS[node_name]
mon.disk_image = UBUNTU_IMG
mon.Desire("rf-controlled", 1)
matrix_nodes[node_name] = mon

rf_ifaces = {}
for node_name, node in matrix_nodes.items():
    for rf_iface_name in RF_IFACES[node_name].values():
        rf_ifaces[rf_iface_name] = node.addInterface(rf_iface_name)

for rf_link_name, rf_iface_names in RF_LINK_NAMES.items():
    rf_link = request.RFLink(rf_link_name)
    for iface_name in rf_iface_names:
        rf_link.addInterface(rf_ifaces[iface_name])


tour = ig.Tour()
tour.Description(ig.Tour.MARKDOWN, tourDescription)
tour.Instructions(ig.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
